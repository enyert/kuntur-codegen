package com.pacifico.kuntur.codegen.exception;

/**
 * Base KunturCodegenException
 */
@SuppressWarnings("serial")
public class KunturCodegenException extends RuntimeException {

    public KunturCodegenException(String message, Throwable cause) {
        super(message, cause);
    }

    public KunturCodegenException(String message) {
        super(message);
    }
}
