package com.pacifico.kuntur.codegen.exception;

public class InvalidVersionException extends RuntimeException {
    public InvalidVersionException(String message) {
        super(message);
    }

    public InvalidVersionException(String message, Throwable cause) {
        super(message, cause);
    }
}
