package com.pacifico.kuntur.codegen.renderer;

import com.pacifico.kuntur.codegen.model.TemplateType;
import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import com.samskivert.mustache.Mustache.Compiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

public class KunturTemplateRenderer {

    private static final Logger log = LoggerFactory.getLogger(KunturTemplateRenderer.class);

    private boolean cache = true;

    private final Compiler mustache;

    private final ConcurrentMap<String, Template> templateCaches = new ConcurrentReferenceHashMap<>();


    public KunturTemplateRenderer(TemplateType templateType) {
        this(mustacheCompiler(templateType));
    }

    public KunturTemplateRenderer(Compiler mustache) {
        this.mustache = mustache;
    }

    public boolean isCache() {
        return cache;
    }

    public void setCache(boolean cache) {
        this.cache = cache;
    }

    public String process(String name, Map<String, ?> model) {
        try {
            Template template = getTemplate(name);
            return template.execute(model);
        }
        catch (Exception ex) {
            log.error("Cannot render: " + name, ex);
            throw new IllegalStateException("Cannot render template", ex);
        }
    }

    public Template getTemplate(String name) {
        if (this.cache) {
            return this.templateCaches.computeIfAbsent(name, this::loadTemplate);
        }
        return loadTemplate(name);
    }

    protected Template loadTemplate(String name) {
        try {
            Reader template;
            template = this.mustache.loader.getTemplate(name);
            return this.mustache.compile(template);
        }
        catch (Exception ex) {
            throw new IllegalStateException("Cannot load template " + name, ex);
        }
    }

    private static Compiler mustacheCompiler(TemplateType templateType) {
        return Mustache.compiler().withLoader(mustacheTemplateLoader(templateType));
    }

    private static Mustache.TemplateLoader mustacheTemplateLoader(TemplateType templateType) {
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        String prefix = "classpath:/templates/" + templateType.type;
        Charset charset = Charset.forName("UTF-8");
        return (name) -> new InputStreamReader(
                resourceLoader.getResource(prefix + name).getInputStream(), charset);
    }
}
