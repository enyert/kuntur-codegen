package com.pacifico.kuntur.codegen.model;

import java.util.List;

public class ProjectType {

    private String name;
    private String description;
    private List<Dependency> baseDependencyList;
    private List<Dependency> featureDependencyList;

    public ProjectType(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Dependency> getBaseDependencyList() {
        return baseDependencyList;
    }

    public void setBaseDependencyList(List<Dependency> baseDependencyList) {
        this.baseDependencyList = baseDependencyList;
    }

    public List<Dependency> getFeatureDependencyList() {
        return featureDependencyList;
    }

    public void setFeatureDependencyList(List<Dependency> featureDependencyList) {
        this.featureDependencyList = featureDependencyList;
    }
}
