package com.pacifico.kuntur.codegen.model;

public enum ScopeType {
    COMPILE("compile"),
    COMPILE_ONLY("compileOnly"),
    RUNTIME("runtime"),
    PROVIDED("provided"),
    TEST("test");

    private String scopeType;

    ScopeType(String scopeType) {
        this.scopeType = scopeType;
    }
}
