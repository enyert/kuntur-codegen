package com.pacifico.kuntur.codegen.model;

public class Plugin {
    String id;
    String version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "id " + this.id + " plugin " + this.version;
    }
}
