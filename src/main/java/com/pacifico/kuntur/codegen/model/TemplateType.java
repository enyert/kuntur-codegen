package com.pacifico.kuntur.codegen.model;

public enum TemplateType {
    CONFIG("config"),
    SOURCE("source"),
    WRAPPER("wrapper");

    public String type;

    TemplateType(String type) {
        this.type = type;
    }
}
