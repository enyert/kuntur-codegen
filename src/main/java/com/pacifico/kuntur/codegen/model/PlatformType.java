package com.pacifico.kuntur.codegen.model;

public enum PlatformType {
    MAVEN,
    GRADLE,
    ANT;
}
