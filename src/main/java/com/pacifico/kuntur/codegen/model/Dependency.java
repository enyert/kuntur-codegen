package com.pacifico.kuntur.codegen.model;

/**
 * Class to define the model for the dependency resource
 */
public class Dependency {

    private String groupId;
    private String version;
    private String artifactId;
    private ScopeType scopeType;

    public Dependency() {
    }

    /**
     * Getter for groupId attribute
     * @return
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * Setter for groupId attribute
     * @param groupId
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * Getter for version attribute
     * @return
     */
    public String getVersion() {
        return version;
    }

    /**
     * Setter for version attribute
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Getter for artifactId attribute
     * @return
     */
    public String getArtifactId() {
        return artifactId;
    }

    /**
     * Setter for artifactId attribute
     * @param artifactId
     */
    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    /**
     * Getter for scopeType attribute
     * @return
     */
    public ScopeType getScopeType() {
        return scopeType;
    }

    /**
     * Setter for scopeType attribute
     * @param scopeType
     */
    public void setScopeType(ScopeType scopeType) {
        this.scopeType = scopeType;
    }
}
