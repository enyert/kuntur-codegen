package com.pacifico.kuntur.codegen.format;

import com.pacifico.kuntur.codegen.model.Dependency;
import com.pacifico.kuntur.codegen.model.PlatformType;
import com.pacifico.kuntur.codegen.model.Plugin;
import com.pacifico.kuntur.codegen.model.Repository;

import java.util.List;

/**
 * This class is an abstraction over the different configuration platforms
 */
public abstract class PlatformConfigFormat {

    public PlatformConfigFormat(PlatformType platformType) {
        this.platformType = platformType;
    }

    private PlatformType platformType;

    public abstract String formatDependencies(List<Dependency> dependencies);
    public abstract String formatPlugins(List<Plugin> plugins);
    public abstract String formatRepository(List<Repository> repositories);


    public PlatformType getPlatformType() {
        return platformType;
    }

    public void setPlatformType(PlatformType platformType) {
        this.platformType = platformType;
    }

    public String buildConfig() {
        //TODO Implementation
        return "This is the config";
    }
}

