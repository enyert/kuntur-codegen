package com.pacifico.kuntur.codegen.format;

import com.pacifico.kuntur.codegen.model.PlatformType;

public class ConfigPlatformFactory {
    public static PlatformConfigFormat getConfigPlatform(PlatformType platformType) {
        PlatformConfigFormat result = new AntPlatformConfig(platformType);
        if(platformType.equals(PlatformType.GRADLE)) {
            result = new GradlePlatformConfig(platformType);
        } else if(platformType.equals(PlatformType.MAVEN)) {
            result = new MavenPlatformConfig(platformType);
        }

        return result;
    }
}
