package com.pacifico.kuntur.codegen.format;

import com.pacifico.kuntur.codegen.model.Dependency;
import com.pacifico.kuntur.codegen.model.PlatformType;
import com.pacifico.kuntur.codegen.model.Plugin;
import com.pacifico.kuntur.codegen.model.Repository;

import java.util.List;

public class AntPlatformConfig extends PlatformConfigFormat {
    public AntPlatformConfig(PlatformType platformType) {
        super(platformType);
    }

    @Override
    public String formatDependencies(List<Dependency> dependencies) {
        return null;
    }

    @Override
    public String formatPlugins(List<Plugin> plugins) {
        return null;
    }

    @Override
    public String formatRepository(List<Repository> repositories) {
        return null;
    }
}
